// Always use an IIFE, i.e., (function() {})();
(function () {
    
    angular
        .module("CWS", [ "ngMessages", "ngAnimate", "ui.router", "ngFileUpload" ] );
})();

// The ngMessages module provides a simple way to show/hide error messages within your form
// It works in conjunction with the ngModel $error object.
// ngAnimate module supports both CSS-based and JS-based animations via callback hooks
// ui-router is a client-side Single Page Application routing framework for AngularJS

