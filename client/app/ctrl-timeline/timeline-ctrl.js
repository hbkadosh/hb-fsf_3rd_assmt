// timeline-ctrl

// (functio() {}) ();  IIFE

(function() {
      angular
        .module("CWS")
        .controller("TimelineCtrl", TimelineCtrl);

    TimelineCtrl.$inject=['TimelineService', 'Upload']; 
    
    // every Angular application must have at least one Controller
  function TimelineCtrl(TimelineService, Upload) { // link TimelineService into TimelineCtrl
        timelineCtrl = this;
        
        timelineCtrl.commentArray = [];

        // timelineCtrl.upload = upload;  // expose function to timelineCtrl


        timelineCtrl.addArr = function() {
            console.log("Timeline CTRL: -> pushing into Array...");
            timelineCtrl.commentArray.push(timelineCtrl); // user-timestamp-comment
            console.log("Timeline CTRL: Array>>> %s", timelineCtrl.commentArray);
            console.log("Timeline CTRL: Array comment>>> %s", timelineCtrl.comment);
            console.log("Timeline CTRL: Array commentID>>> %s", timelineCtrl.commenterID);
            console.log("Timeline CTRL: Array commentName>>> %s", timelineCtrl.updated_at);

            console.log("Timeline CTRL: type-> %s", timelineCtrl);
        }

    var initForm = function(timelineCtrl) {
        console.log("initForm start");
        timelineCtrl.created_at = "";
        timelineCtrl.updated_at = "";
        timelineCtrl.commenterID = "";
        timelineCtrl.commenterName = "";
        timelineCtrl.comment = "";
        timelineCtrl.fileUpLoadID = "";
        console.log("initForm end");
    }

    var commentQueryObject = function(timelineCtrl) {
        // timelineCtrl.commenterID = "test-ID";
        // timelineCtrl.commenterName = "testing commenterName";
        return({
            created_at: new Date(),  // temporary testing
            updated_at: new Date(),  // new Date(commentCtrl.timestamp), // date
            commenterID: timelineCtrl.commenterID,
            commenterName: timelineCtrl.commenterName,
            comment: timelineCtrl.comment,
            fileUpLoadID: timelineCtrl.fileUpLoadID
        })
    }


    initForm(timelineCtrl);
    console.log("CTRL >> after initForm()");
    
    timelineCtrl.showComment = function() {
        console.log("Timeline CTRL: inside timelineCtrl.showComment...");
        timelineCtrl.comments = [];
        TimelineService.showComment()
                .then(function(allComments) { // returns from MySQL an ArrayObj of Objects
                    // show allComments in reverse order (LIFO)
                    var temp = allComments.data;
                    var showdate = "";
                    // console.log("Timeline CTRL >> %s %s", allComments, temp);
                    console.log("Timeline CTRL allComments >> ", allComments);
                    console.log("Timeline CTRL allComments.data >> ", allComments.data);
                    allComments.data = temp.reverse();
                    // var showdate = str.substring(0, 10) + "/" + str.substring(11, 19);
                    for (var i = 0; i < allComments.data.length; i++) {
                        showdate = allComments.data[i].created_at.substring(0, 10) + " / " + allComments.data[i].created_at.substring(11, 19);
                        allComments.data[i].created_at = showdate;
                    };
                    timelineCtrl.comments = allComments.data;
                    console.log("Timeline CTRL date-str: ", timelineCtrl.comments[0].created_at);
                    console.log("Timeline CTRL timelineCtrl.comments: ", timelineCtrl.comments);
        })            
    }

    console.log("CTRL >> before showComment()");
    timelineCtrl.showComment();
    console.log("CTRL >> after showComment()");


    // load image from server
    // timelineCtrl.loadImage = function() {
    //     console.log("Timeline CTRL: inside timelineCtrl.loadImage...");
    //     timelineCtrl.images = TimelineService.loadImage()
    //             .then(function(allImages) {
    //                 timelineCtrl.images = allImages;
    //                 console.log("Timeline CTRL: ", allImages);
    //     })            
    // }

    // using CommentSvc ($http) to send data to server
    timelineCtrl.placeComment = function() {
        console.log("Timeline CTRL: inside timelineCtrl.placeComment...");
        // timelineCtrl.showComment();

        // timelineCtrl.date = (new Date()).toString(); // set time to comment
        // timelineCtrl.user & timelineCtrl.comment & timelineCtrl.image entered at HTML
        timelineCtrl.addArr(); // push object into array
        timelineCtrl.showComment();
        TimelineService.placeComment(commentQueryObject(timelineCtrl));
        console.log("Timeline CTRL: end of timelineCtrl.placeComment");


        // TimelineService.placeComment(commentQueryObject(timelineCtrl))
        //     .then(function() {
        //         timelineCtrl.showComment();
        //         console.log("Timeline CTRL: after showComment()");
        //         initForm(timelineCtrl);
        //     });
    }

    // File uploading function
    timelineCtrl.imgFile = null;
    timelineCtrl.status = {
        message: "",
        code: 0
    };

    timelineCtrl.upload = function() {
        console.log("CTRL inside timelineCtrl.upload");
        // console.log("CTRL timelineCtrl upload >> #1 original imgFile: %s", timelineCtrl.imgFile.name);
        // timelineCtrl.fileUpLoadID = Date.now() + '-' + timelineCtrl.imgFile.name;
        // console.log("CTRL timelineCtrl upload >> #2 fileUpLoadID: %s", timelineCtrl.fileUpLoadID);
        // timelineCtrl.imgFile.name = timelineCtrl.fileUpLoadID;
        console.log("image fiel");
        console.log(timelineCtrl);

        
        console.log(timelineCtrl.imgFile);
        console.log("CTRL timelineCtrl upload >> #3 changed imgFile: %s", timelineCtrl.imgFile.name);

        TimelineService.insertData(timelineCtrl.imgFile, commentQueryObject(timelineCtrl));
        // TimelineService
        // .upload(timelineCtrl.imgFile)
        //     // if upload successful (should be put inside the .then() statement)
        //     .then(function(result){
        //         timelineCtrl.fileUpLoadID = '';
        //         timelineCtrl.imgFile = null;
        //     })
            // .then(function(result) {
            //     console.log("CTRL upload() >> img filename: ", result.data)
            //     timelineCtrl.fileurl = result.data;
            //     timelineCtrl.status.message = "Upload CTRL >> Success";
            //     timelineCtrl.status.code = 202;            
            // })
            // .catch(function(err){
            //     console.log(err);
            //     timelineCtrl.status.message = "Upload CTRL >> Error";
            //     timelineCtrl.status.code = 500;
            // });
    }

    // function upload(){
    //     console.log("CTRL upload() >> ");

    //     Upload
    //         .upload({     // SVC
    //             url:'/upload',
    //             data: { "img-file": timelineCtrl.imgFile }   // <object> data.img-file
    //         })
    //         .then(function(result) {
    //             console.log("CTRL upload() >> img filename: ", result.data)
    //             timelineCtrl.fileurl = result.data;
    //             timelineCtrl.status.message = "Upload CTRL >> Success";
    //             timelineCtrl.status.code = 202;            
    //         })
    //         .catch(function(err){
    //             console.log(err);
    //             timelineCtrl.status.message = "Upload CTRL >> Error";
    //             timelineCtrl.status.code = 500;
    //         })
    // }
        

    
}    

   
}) ();








