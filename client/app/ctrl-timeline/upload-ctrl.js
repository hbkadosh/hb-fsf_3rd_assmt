// upload-ctrl.js

(function(){
    angular
        .module("CWS")
        .controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["TimelineService"]   // ["Upload"];

    function UploadCtrl(Upload) {
        console.log("CTRL Upload >>");

        var vm = this;

        vm.imgFile = null;
        vm.status = {
            message: "",
            code: 0
        };

        vm.upload = upload;  // expose function to vm

        function upload(){
            console.log("CTRL upload() >> ");

            Upload
            .upload({     // SVC
                url:'/upload',
                data: {
                    "img-file": vm.imgFile,
                }
            })
            .then(function(result) {
                console.log("CTRL upload() >> img filename: ", result.data)
                vm.fileurl = result.data;
                vm.status.message = "Upload CTRL >> Success";
                vm.status.code = 202;            
            })
            .catch(function(err){
                console.log(err);
                vm.status.message = "Upload CTRL >> Error";
                vm.status.code = 500;
            })
        }
        
    } // end of Controller
    
}) ();
