// Defines client-side routing
(function () {
    angular
        .module("CWS")
        .config(cwsRouteConfig);


console.log("in router");

    cwsRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function cwsRouteConfig($stateProvider, $urlRouterProvider) {
        console.log("APP.CONFIG-FSF.JS >>");

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: './app/ctrl-login/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state('timeline', {
                url: '/timeline',
                templateUrl: './app/ctrl-timeline/timeline.html',
                // parent: 'login',
                controller: 'TimelineCtrl',
                controllerAs: 'ctrl'   
            })
            .state('messages', {
                url: '/messages',
                // parent: 'timeline',
                templateUrl: './app/ctrl-message/messages.html'
            //     // controller: 'MessageCtrl',
            //     // controllerAs: 'ctrl'
            });
            // .state('analytics', {
            //     url: '/analytics',
            //     templateUrl: './app/ctrl-timeline/timeline.html', ??? separate directory?
            //     controller: 'TimelineCtrl',  ??? separate ctrl?
            //     controllerAs: 'ctrl'
            // });
            // ******************************************************************
            // .state('editWithParam', {
            //     url: '/edit/:pdtNo',
            //     templateUrl: './app/edit/edit.html',
            //     controller: 'EditCtrl',
            //     controllerAs: 'ctrl'
            // })  
            // .state('thanks', {
            //     url: '/thanks',
            //     templateUrl: './app/registration/thanks.html'
            // })

        $urlRouterProvider.otherwise("/login");
    }

})();

