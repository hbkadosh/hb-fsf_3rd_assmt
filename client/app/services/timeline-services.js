// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches TimelineService service to the CWS module
    angular
        .module("CWS")
        .service("TimelineService", TimelineService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    TimelineService.$inject = ['$http', 'Upload'];

    // TimelineService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function TimelineService($http, Upload) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        var userLoginID = "";
        var userEmail = "";
        var searchString = "";

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.placeComment = placeComment;
        service.showComment = showComment;
        service.retrieveLogin = retrieveLogin;
        // service.upload = upload;
        service.insertData = insertData;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        function retrieveLogin(s) {
            console.log("SVC in timeline-services.js >>");
            console.log("SVC Login >> retrieveLogin() s :", s);
            userLoginID = s.id;
            userEmail = s.email;
            console.log("SVC Login >> userLoginID %s", userLoginID);

            return $http({
                method: 'POST',
                url: '/api/login',
                data : { str: s } 
            });
        }


        function placeComment(commentDetail) {
            commentDetail.commenterID = userLoginID;
            commentDetail.commenterName = userEmail;

            console.log("SVC: placeComment()---");
            console.log("SVC: commentDetail: ", commentDetail);

            console.log("userID: %s", commentDetail.commenterID);   // LoginID.id);
            console.log("userName: %s", commentDetail.commenterName);
            console.log("updated_at: %s", commentDetail.updated_at);

            // return ($http.get("/placeComment", {
            //     params: commentDetail  
            // }));
            // return $http({
            //     method: 'POST',
            //     url: 'comments',
            //     data: { cmt: commentDetail } // body of response messageS 
            // });
        }


        // retrieves comment details for viewing
        function showComment() {
            console.log("SVC: showComment()...");
            var commentDetail = "testing";
            console.log("SVC commentDetail: ", commentDetail);
            
            // return ($http.get("/showComment")
            //     .then(function(result) {
            //         console.log("Comment SVC: >>> Results: %s", result);
            //         return (result.data);
            // }));
            return $http({
                method: 'GET',
                url: 'comments',
                params : { 'searchString': searchString }
                // data: { cmt: commentDetail } // body of response msg to control GET's content
            });
        }


        // inserts/creates both comment details and file upload
        function insertData(imgFile, commentDetail){
            commentDetail.commenterID = userLoginID;
            commentDetail.commenterName = userEmail;
            console.log("insert data");
            console.log(imgFile);
            console.log(commentDetail);
            Upload                      // SVC
                .upload({     
                    url:'/insertData',
                    data: 
                    { "imgf": imgFile ,
                       'cmt': commentDetail
                    }                      
                })
                .then(function(result) {
                    console.log("SVC upload() >> img filename: ", result.data);
                })
                .catch(function(err){
                    console.log(err);
                });
        }


        // function upload(imagefile){
        //     console.log("SVC: upload() >> ");
        //     console.log(imagefile);

        //     // return $http({
        //     //         method: 'POST',
        //     //         url: 'upload',
        //     //         data: { imgf: imagefile } // body of response messageS 
        //     // });
            
        //     Upload
        //         .upload({     // SVC
        //             url:'/upload',
        //             data: { "imgf": timelineCtrl.imgFile }   // <object> data.img-file               
        //         })
        //         .then(function(result) {
        //             console.log("SVC upload() >> img filename: ", result.data)
        //             timelineCtrl.fileurl = result.data;
        //             timelineCtrl.status.message = "SVC Upload >> Success";
        //             timelineCtrl.status.code = 202;            
        //         })
        //         .catch(function(err){
        //             console.log(err);
        //             timelineCtrl.status.message = "SVC Upload >> Error";
        //             timelineCtrl.status.code = 500;
        //         })
        // }
        
    }

})();

