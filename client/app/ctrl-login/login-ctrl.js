(function () {
    angular
        .module("CWS")
        .controller("LoginCtrl", LoginCtrl);

    LoginCtrl.$inject = ['$state', 'TimelineService'];

    function LoginCtrl($state, TimelineService) {
        var vm = this;
        
        console.log("CTRL Login at vm >> ", vm); 
        console.log("CTRL Login at vm.login >> ", JSON.stringify(vm.login)); 

        // vm.searchString = '';
        vm.result = null;

        console.log("CTRL Login >> from Login.html"); // shows only 1st time on loading into memory
        console.log("CTRL Login at vm >> ", vm); 
        console.log("CTRL Login at vm.login >> ", JSON.stringify(vm.login));
        //     ng-model= "ctrl.login.id"
        //     ng-model= "ctrl.login.email"
        //     ng-model= "ctrl.login.password"

        // Exposed FUNCTIONS **************************************************** 
        // Exposed functions can be called from the view.

        // vm.goEdit = goEdit;
        vm.search = search;

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)

        // init();

        // Function declaration and definition -------------------------------------------------------------------------
        // function goEdit(deptNo){
        //     $state.go("editWithParam",{deptNo : deptNo});
        // }


        // The init function initializes view
        function init() {
            console.log("CTRL Login >> Init() start");
            
            TimelineService
                .retrieveLogin('')
                .then(function (results) {
                    console.log("CTRL Login Init() Success");
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.searchString = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("CTRL Login Init() error " + err);
                });
        }


        function search() {
            console.log("CTRL Login >> Search() start");
            console.log("CTRL Login at vm.login >> ", JSON.stringify(vm.login));
            console.log("CTRL Login at vm.login >> %s %s %s", vm.login.id, vm.login.email, vm.login.password);  

            TimelineService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveLogin(vm.login)
                .then(function (results) {
                    console.log("CTRL Login Search() Success results %s", JSON.stringify(results));
                    console.log("CTRL Login Search() Success results.data %s", results.data);
                    // for (var i in result.data) console.log("CTRL Login Search() Success results.data %s", results.data[i]);
                    
                    if (results.data == "") {
                        console.log("CTRL Login Search() data=blank");
                    }
                    if (results.data != "") {
                        console.log("CTRL Login Search() data=ok");

                        vm.gototimeline = gototimeline;

                        function gototimeline(){
                            console.log("CTRL Login >> gototimeline");
                            $state.go('timeline');
                        }
                    }

                    vm.searchString = results.data; // ???                    
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("CTRL Login Search() error " + err);
                });
        }
    }
})();