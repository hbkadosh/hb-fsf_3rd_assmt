// timeline-model.js
// Creates a model for timelinecommenttbl table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (sequelize, Sequelize) {
    var TimelineModel = sequelize.define('timeline', {
            // id: {
            //     type: Sequelize.INTEGER(11),
            //     allowNull: false,
            //     primaryKey: true
            // },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false,
                primaryKey: true
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            commenterID: {
                type: Sequelize.STRING(30),
                allowNull: false,
                // primaryKey: true
            },
            commenterName: {
                type: Sequelize.STRING(30),
                allowNull: true
            },
            comment: {
                type: Sequelize.STRING(255),
                allowNull: true
            },
            fileUpLoadID: {
                type: Sequelize.STRING(100),
                allowNull: true
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "timelinecommenttbl"
        }
    );

    return TimelineModel;
};