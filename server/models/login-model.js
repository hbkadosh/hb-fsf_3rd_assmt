// login-model.js
// Creates a model for logintbl table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
module.exports = function (conn, Sequelize) {
    var LoginModel = conn.define('login', {
            loginID: {
                type: Sequelize.STRING(30),
                allowNull: false,
                primaryKey: true
            },
            loginEmail: {
                type: Sequelize.STRING(50),
                allowNull: false
            },
            loginPassword: {
                type: Sequelize.STRING(30),
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            tableName: "logintbl"
        }
    );
    console.log("MODEL LoginModel >> ", LoginModel);
    return LoginModel;
};