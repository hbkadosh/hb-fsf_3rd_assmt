var express = require('express');
var app = express();

var path = require('path');

var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

var config = require('./config');
var db = require('./db');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(session({
    secret: config.session_secret,
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

require('./routes')(app, passport);
require('./auth')(db, app, passport);

app.listen(config.port, function() {
  console.log("Web server started on port %d...", config.port);
});